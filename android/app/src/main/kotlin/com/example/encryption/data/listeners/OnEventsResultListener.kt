package com.example.encryption.data.listeners

import androidx.annotation.UiThread
import com.example.encryption.data.methods.Method

interface OnEventsResultListener : OnMethodResultListener {

    @UiThread
    fun onDone(method: Method)

}
