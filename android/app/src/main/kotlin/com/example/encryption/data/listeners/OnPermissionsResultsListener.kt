package com.example.encryption.data.listeners

interface OnPermissionsResultsListener {

    fun onPermissionRequestResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) = false

}
