package com.example.encryption.data.listeners

import android.content.Intent

interface OnActivityResultListener {

    /**
     * Return [true] if result was consumed
     */
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean = false

}
