package com.example.encryption

import android.content.Intent
import com.example.encryption.data.ENCRYPTION_PROGRESS_CHANNEL
import com.example.encryption.data.MAIN_METHOD_CHANNEL
import com.example.encryption.data.MethodManager
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodChannel

class MainActivity: FlutterActivity() {

    private val methodManager = MethodManager(this)

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        EventChannel(flutterEngine.dartExecutor, ENCRYPTION_PROGRESS_CHANNEL).setStreamHandler(
            object : EventChannel.StreamHandler {
                override fun onListen(arguments: Any, events: EventChannel.EventSink) {
                    methodManager.initStream(ENCRYPTION_PROGRESS_CHANNEL, arguments, events)
                }

                override fun onCancel(arguments: Any) {
                    methodManager.disposeStream(ENCRYPTION_PROGRESS_CHANNEL)
                }
            }
        )
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, MAIN_METHOD_CHANNEL).setMethodCallHandler {
            call, result -> methodManager.getMethod(result, call)?.invoke() ?: result.notImplemented()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!methodManager.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (!methodManager.onPermissionRequestResult(requestCode, permissions, grantResults)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

}
