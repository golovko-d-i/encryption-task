package com.example.encryption.data

const val MAIN_METHOD_CHANNEL = "example.com/encryption"
const val ENCRYPTION_PROGRESS_CHANNEL = "example.com/encryption_progress"

object ErrorCode {
    const val USER_CANCELLED = "user_cancelled"
    const val PERMISSION_REQUEST_DECLINED = "permission_request_declined"
    const val INACCESSIBLE_FILE = "inaccessible_file"
    const val CANNOT_PICK_FILE = "cannot_pick_file"
    const val INCORRECT_PARAMS = "incorrect_params"
    const val AN_ERROR_OCCURRED = "an_error_occurred"
}
