package com.example.encryption.data.utils

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.Settings
import androidx.annotation.RequiresApi
import com.example.encryption.data.ErrorCode
import com.example.encryption.data.listeners.OnActivityResultListener
import com.example.encryption.data.listeners.OnPermissionsResultsListener

private const val PICK_FILE_REQUEST_CODE = 632
private const val MANAGE_ALL_FILES_REQUEST_CODE = 235
private const val STORAGE_PERMISSION_REQUEST_CODE = 264
private const val STORAGE_WRITE_PERMISSION_REQUEST_CODE = 643

class RequestsManager(
    private val permissionUtils: PermissionUtils,
    private val activity: Activity
) : OnActivityResultListener, OnPermissionsResultsListener {

    private val methodListeners = mutableMapOf<Int, Listeners>()

    fun checkAndRequestReadStorage(
        onSuccess: () -> Unit,
        onError: () -> Unit
    ) {
        if (permissionUtils.isApi30) {
            @RequiresApi(Build.VERSION_CODES.R)
            if (Environment.isExternalStorageManager()) {
                onSuccess()
            } else {
                addUnitListeners(MANAGE_ALL_FILES_REQUEST_CODE, onSuccess) { onError() }
                val intent = Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION)
                activity.startActivityForResult(intent, MANAGE_ALL_FILES_REQUEST_CODE)
            }
        } else {
            if (permissionUtils.hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                onSuccess()
            } else {
                addUnitListeners(STORAGE_PERMISSION_REQUEST_CODE, onSuccess) { onError() }
                permissionUtils.requestPermission(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    STORAGE_PERMISSION_REQUEST_CODE
                )
            }
        }
    }

    fun checkAndRequestWriteStorage(
        onSuccess: () -> Unit,
        onError: () -> Unit
    ) {
        if (permissionUtils.isApi30) {
            @RequiresApi(Build.VERSION_CODES.R)
            if (Environment.isExternalStorageManager()) {
                onSuccess()
            } else {
                addUnitListeners(MANAGE_ALL_FILES_REQUEST_CODE, onSuccess) { onError() }
                val intent = Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION)
                activity.startActivityForResult(intent, MANAGE_ALL_FILES_REQUEST_CODE)
            }
        } else {
            if (permissionUtils.hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                onSuccess()
            } else {
                addUnitListeners(STORAGE_WRITE_PERMISSION_REQUEST_CODE, onSuccess) { onError() }
                permissionUtils.requestPermission(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    STORAGE_WRITE_PERMISSION_REQUEST_CODE
                )
            }
        }
    }

    fun requestPickFile(
        onSuccess: (Uri?) -> Unit,
        onError: (String?) -> Unit
    ) {
        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
            type = "*/*"
            putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        }
        if (intent.resolveActivity(activity.packageManager) != null) {
            methodListeners[PICK_FILE_REQUEST_CODE] = Listeners.DataListeners(onSuccess, onError)
            activity.startActivityForResult(intent, PICK_FILE_REQUEST_CODE)
        } else {
            onError(ErrorCode.CANNOT_PICK_FILE)
        }
    }

    private fun addUnitListeners(
        code: Int,
        onSuccess: () -> Unit,
        onError: (String?) -> Unit
    ) {
        methodListeners[code] = Listeners.UnitListeners(onSuccess, onError)
    }

    private fun removeListeners(code: Int) {
        methodListeners.remove(code)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        return methodListeners[requestCode]?.let {
            when (requestCode) {
                MANAGE_ALL_FILES_REQUEST_CODE -> {
                    (it as? Listeners.UnitListeners)?.let { listeners ->
                        @RequiresApi(Build.VERSION_CODES.R)
                        if (Environment.isExternalStorageManager()) {
                            listeners.onSuccess()
                        } else {
                            listeners.onError(null)
                        }
                    }
                }
                PICK_FILE_REQUEST_CODE -> {
                    (it as? Listeners.DataListeners<Uri?>)?.let { listeners ->
                        if (resultCode == Activity.RESULT_OK) {
                            listeners.onSuccess(data?.data)
                        } else {
                            listeners.onError(ErrorCode.USER_CANCELLED)
                        }
                    }
                }
            }
            removeListeners(requestCode)
            true
        } ?: false
    }

    override fun onPermissionRequestResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ): Boolean {
        return methodListeners[requestCode]
            ?.takeIf { it is Listeners.UnitListeners }
            ?.let {
                val listeners = it as Listeners.UnitListeners
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    listeners.onSuccess()
                } else {
                    listeners.onError(null)
                }
                removeListeners(requestCode)
                true
            } ?: false
    }

    sealed class Listeners {
        abstract val onError: (String?) -> Unit

        data class UnitListeners(
            val onSuccess: () -> Unit,
            override val onError: (String?) -> Unit
        ) : Listeners()

        data class DataListeners<T>(
            val onSuccess: (T) -> Unit,
            override val onError: (String?) -> Unit
        ) : Listeners()
    }

}
