package com.example.encryption.data.listeners

import androidx.annotation.UiThread
import com.example.encryption.data.methods.Method

interface OnMethodResultListener {

    @UiThread
    fun onSuccess(method: Method, data: Any?)

    @UiThread
    fun onError(method: Method, errorCode: String, errorMessage: String? = null)

}
