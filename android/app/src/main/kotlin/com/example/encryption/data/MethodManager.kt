package com.example.encryption.data

import android.app.Activity
import android.content.Intent
import com.example.encryption.data.listeners.OnActivityResultListener
import com.example.encryption.data.listeners.OnEventsResultListener
import com.example.encryption.data.listeners.OnMethodResultListener
import com.example.encryption.data.listeners.OnPermissionsResultsListener
import com.example.encryption.data.methods.EncryptFile
import com.example.encryption.data.methods.Method
import com.example.encryption.data.methods.PickFile
import com.example.encryption.data.utils.FileUtils
import com.example.encryption.data.utils.PermissionUtils
import com.example.encryption.data.utils.RequestsManager
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

private const val PICK_FILE_METHOD = "pickFile"

/**
 * Manages all platform-specific code, activity and permissions results callbacks
 */
class MethodManager(activity: Activity) : OnActivityResultListener, OnPermissionsResultsListener {

    private val fileUtils = FileUtils(activity)
    private val permissionUtils = PermissionUtils(activity)
    private val requestsManager = RequestsManager(permissionUtils, activity)

    private val streamsInProgress = mutableMapOf<String, Method>()

    fun initStream(
        channel: String,
        arguments: Any,
        events: EventChannel.EventSink
    ) {
        val method = when (channel) {
            ENCRYPTION_PROGRESS_CHANNEL -> {
                (arguments as? List<String>)?.let {
                    EncryptFile(it, requestsManager, EventsResultListener(events))
                }
            }
            else -> null
        }
        if (method != null) {
            streamsInProgress[channel] = method
            method.execute()
        } else {
            events.endOfStream()
        }
    }

    fun disposeStream(channel: String) {
        streamsInProgress.remove(channel)
    }

    fun getMethod(
        methodResult: MethodChannel.Result,
        method: MethodCall
    ): (() -> Unit)? = (when (method.method) {
        PICK_FILE_METHOD -> {
            PickFile(fileUtils, requestsManager, MethodResultListener(methodResult))
        }
        else -> null
    })?.let { it::execute }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        return requestsManager.onActivityResult(requestCode, resultCode, data)
    }

    override fun onPermissionRequestResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ): Boolean {
        return requestsManager.onPermissionRequestResult(requestCode, permissions, grantResults)
    }

    /**
     * Results
     */
    private class MethodResultListener(
        private val methodResult: MethodChannel.Result
    ) : OnMethodResultListener {

        override fun onSuccess(method: Method, data: Any?) {
            methodResult.success(data)
        }

        override fun onError(method: Method, errorCode: String, errorMessage: String?) {
            methodResult.error(errorCode, errorMessage, null)
        }
    }

    private class EventsResultListener(
        private val eventsSink: EventChannel.EventSink
    ) : OnEventsResultListener {

        override fun onSuccess(method: Method, data: Any?) {
            eventsSink.success(data)
        }

        override fun onError(method: Method, errorCode: String, errorMessage: String?) {
            eventsSink.error(errorCode, errorMessage, null)
        }

        override fun onDone(method: Method) {
            eventsSink.endOfStream()
        }
    }
}
