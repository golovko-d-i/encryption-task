package com.example.encryption.data.methods

import androidx.annotation.UiThread
import com.example.encryption.data.listeners.OnEventsResultListener

abstract class BaseEventsMethod(
    private val onMethodResultListener: OnEventsResultListener
) : BaseMethod(onMethodResultListener) {

    @UiThread
    fun onDone() = onMethodResultListener.onDone(this)

}
