package com.example.encryption.data.methods

import android.net.Uri
import com.example.encryption.data.ErrorCode
import com.example.encryption.data.listeners.OnMethodResultListener
import com.example.encryption.data.utils.FileUtils
import com.example.encryption.data.utils.RequestsManager

private const val PICK_FILE_REQUEST_CODE = 632
private const val MANAGE_ALL_FILES_REQUEST_CODE = 235
private const val STORAGE_PERMISSION_REQUEST_CODE = 264

/**
 * File picker method.
 * Requests manage all files or read permission to get an absolute path to selected file.
 * Returns absolute path to selected file.
 * Possible errors:
 * - cannot_pick_file
 * - permission_request_declined
 * - inaccessible_file
 * - user_cancelled
 */
class PickFile(
    private val fileUtils: FileUtils,
    private val requestsManager: RequestsManager,
    onMethodResultListener: OnMethodResultListener
) : BaseMethod(onMethodResultListener) {

    override fun execute() {
        requestsManager.checkAndRequestReadStorage(::openFilePicker) {
            onError(ErrorCode.PERMISSION_REQUEST_DECLINED, "Cannot access file")
        }
    }

    private fun openFilePicker() {
        requestsManager.requestPickFile(::processFileUri) { error ->
            val message = when (error) {
                ErrorCode.USER_CANCELLED -> "User cancelled pick file flow."
                else -> null
            }
            error?.let { onError(it, message) }
        }
    }

    private fun processFileUri(uri: Uri?) {
        val filePath = uri?.let { fileUtils.getPath(it) }
        if (filePath != null) {
            onSuccess(filePath)
        } else {
            onError(ErrorCode.INACCESSIBLE_FILE, "Cannot access file")
        }
    }

}
