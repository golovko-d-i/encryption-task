package com.example.encryption.data.methods

import androidx.annotation.UiThread
import com.example.encryption.data.listeners.OnMethodResultListener

abstract class BaseMethod(
    private val onMethodResultListener: OnMethodResultListener
) : Method {

    @UiThread
    fun onSuccess(data: Any?) = onMethodResultListener.onSuccess(this, data)

    @UiThread
    fun onError(errorCode: String, errorMessage: String? = null) =
        onMethodResultListener.onError(this, errorCode, errorMessage)

}
