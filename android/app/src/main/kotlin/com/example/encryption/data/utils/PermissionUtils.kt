package com.example.encryption.data.utils

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.os.Environment
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat

class PermissionUtils(private val activity: Activity) {

    val isApi30: Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.R

    fun hasPermission(permission: String): Boolean =
        ActivityCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED

    fun requestPermission(permission: String, requestCode: Int) =
        ActivityCompat.requestPermissions(activity, arrayOf(permission), requestCode)

}
