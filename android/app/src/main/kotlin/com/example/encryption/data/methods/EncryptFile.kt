package com.example.encryption.data.methods

import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.annotation.UiThread
import androidx.annotation.WorkerThread
import com.example.encryption.data.ErrorCode
import com.example.encryption.data.listeners.OnEventsResultListener
import com.example.encryption.data.utils.RequestsManager
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.CipherOutputStream
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec


private const val ENCRYPTED_POSTFIX = ".encrypted"
private const val WORD_SIZE = 8
private const val MANAGE_ALL_FILES_REQUEST_CODE = 111
private const val STORAGE_WRITE_PERMISSION_REQUEST_CODE = 643
private const val DELTA_UPDATE_PERCENT = 0.001

/**
 * File encryption method. Uses provided file path, password and encryption type for execution.
 * Requests manage all files or writing permission and saves encrypted file next to the selected file with
 * `.encrypted` suffix
 * Percent of encryption process is sent with each change on at least [DELTA_UPDATE_PERCENT]
 * Possible errors:
 *  - incorrect_params
 *  - an_error_occurred (generic error)
 *  - permission_request_declined
 */
class EncryptFile(
    private val params: List<String>,
    private val requestsManager: RequestsManager,
    resultListener: OnEventsResultListener
) : BaseEventsMethod(resultListener) {

    private val mainThreadHandler = Handler(Looper.getMainLooper())

    private lateinit var fileToEncryptPath: String
    private lateinit var encryptionType: String
    private lateinit var filePassword: String

    override fun execute() {
        val file = params.firstOrNull()
        val encryption = params.getOrNull(1)
        val password = params.getOrNull(2)
        if (file.isNullOrEmpty() || encryption.isNullOrEmpty() || password.isNullOrEmpty()) {
            onError(ErrorCode.INCORRECT_PARAMS)
            onDone()
        } else {
            fileToEncryptPath = file
            encryptionType = encryption
            filePassword = password
            requestsManager.checkAndRequestWriteStorage(::startJob) {
                onError(ErrorCode.PERMISSION_REQUEST_DECLINED)
                onDone()
            }
        }
    }

    @UiThread
    private fun startJob() {
        Thread {
            try {
                encrypt(filePassword, encryptionType, fileToEncryptPath)
            } catch (e: Exception) {
                e.printStackTrace()
                sendError(e)
                sendDone()
            }
        }.start()
    }

    @Throws
    @WorkerThread
    private fun encrypt(
        password: String,
        encryptionType: String,
        fileToEncryptPath: String
    ) {
        val fis = FileInputStream(fileToEncryptPath)
        val fos = FileOutputStream(fileToEncryptPath.encryptedPath)

        val sr = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            SecureRandom.getInstanceStrong()
        } else {
            SecureRandom()
        }
        val salt = ByteArray(16)
        sr.nextBytes(salt)

        val spec = PBEKeySpec(password.toCharArray(), salt, 1000, 32 * 8)
        // key should be saved somewhere too, but
        val key = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1").generateSecret(spec)

        val cipher: Cipher = Cipher.getInstance(encryptionType)
        cipher.init(Cipher.ENCRYPT_MODE, key)

        val cos = CipherOutputStream(fos, cipher)

        val fileSize = File(fileToEncryptPath).length().toDouble()

        val d = ByteArray(WORD_SIZE)
        var b: Int = fis.read(d)
        var bytesRead = WORD_SIZE
        var percent = 0.0
        while (b != -1) {
            val progress = bytesRead / fileSize
            // no need to send it often
            if (progress - percent > DELTA_UPDATE_PERCENT) {
                percent = progress
                sendProgress(percent)
            }
            cos.write(d, 0, b)
            b = fis.read(d)
            bytesRead += WORD_SIZE
        }
        sendProgress(1.0)
        cos.flush()
        cos.close()
        fis.close()
        sendDone()
    }

    @WorkerThread
    fun sendProgress(progress: Double) {
        mainThreadHandler.post {
            this@EncryptFile.onSuccess(progress)
        }
    }

    @WorkerThread
    fun sendError(e: Exception) {
        mainThreadHandler.post {
            this@EncryptFile.onError(ErrorCode.AN_ERROR_OCCURRED, e.message)
        }
    }

    @WorkerThread
    fun sendDone() {
        mainThreadHandler.post {
            onDone()
        }
    }

    private val String.encryptedPath: String
        get() = "$this$ENCRYPTED_POSTFIX"

}
