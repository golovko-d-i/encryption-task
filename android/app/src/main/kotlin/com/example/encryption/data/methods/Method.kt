package com.example.encryption.data.methods

import androidx.annotation.UiThread

/**
 * Base method interface
 */
interface Method {

    /**
     * Starts method execution
     */
    @UiThread
    fun execute()

}
