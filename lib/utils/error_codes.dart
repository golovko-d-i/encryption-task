
class ErrorCode {
  static const String userCancelled = 'user_cancelled';
  static const String permissionRequestDeclined = 'permission_request_declined';
  static const String inaccessibleFile = 'inaccessible_file';
  static const String cannotPickFile = 'cannot_pick_file';
  static const String incorrectParams = 'incorrect_params';
  static const String anErrorOccurred = 'an_error_occurred';
}
