
import 'package:flutter/cupertino.dart';

extension PostDelayedState on State {
  void postDelayed(Function block, {Duration duration = Duration.zero}) {
    _postDelayed(block, duration: duration);
  }
}

void _postDelayed(Function block, {Duration duration = Duration.zero}) {
  Future<void>.delayed(duration, () {
    block.call();
  });
}
