import 'package:encryption/data/result.dart';
import 'package:flutter/services.dart';

import '../platform.dart';

class FileEncryptionProvider {
  static const _pickFileMethod = "pickFile";
  static const encodeFileChannel =
      EventChannel("example.com/encryption_progress");

  /// Calls platform code to select a file in filesystem
  /// [Success] returns absolute path to file
  Future<Result<String>> pickFile() async {
    try {
      var path = await platform.invokeMethod(_pickFileMethod);
      return Success(path);
    } on PlatformException catch (e) {
      return Error(
        errorCode: e.code,
        message: e.message,
      );
    }
  }

  /// Calls platform code to encrypt selected file in filesystem
  /// using provided [encoding] method and [password]
  /// [Success] returns encryption progress, from 0 to 1
  Stream<Result<double>> encodeFile(
    String filePath,
    String encoding,
    String password,
  ) {
    return encodeFileChannel
        .receiveBroadcastStream([filePath, encoding, password]).map((event) {
      return Success<double>(event);
    }).handleError((e) {
      if (e is PlatformException) {
        return Error<double>(errorCode: e.code, message: e.message);
      }
    });
  }
}
