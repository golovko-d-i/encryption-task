
import 'package:encryption/data/provider/file_encryption_provider.dart';
import 'package:provider/provider.dart';

class DataProvider {

  FileEncryptionProvider filePicker = FileEncryptionProvider();

  static DataProvider of(context) => Provider.of(context, listen: false);

}
