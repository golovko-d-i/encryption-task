class Result<T> {}

class Success<T> extends Result<T> {
  final T result;

  Success(this.result);
}

class Error<T> extends Result<T> {
  final String errorCode;
  final String? message;

  Error({required this.errorCode, this.message});
}
