
class Strings {
  static const cannotAccessFile = "Can't access selected file";
  static const genericError = "Something went wrong";
  static const selectFileToEncrypt = "Select file to encrypt";
  static const homeStateError = "An error has occurred. Incorrect state ¯\\_(ツ)_/¯";
  static const appTitle = "Assignment Option 1: AES Encryption";
  static const encryptionComplete = "Encryption complete!";
}
