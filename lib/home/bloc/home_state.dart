part of 'home_bloc.dart';

@immutable
abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeSelectFile extends HomeState {
  final String? message;

  /// optional snackbar [message]
  const HomeSelectFile({this.message});
}

/// Displays file encryption progress
class HomeProgress extends HomeState {
  final int progress;

  const HomeProgress([this.progress = 0]);

  @override
  List<Object> get props => [progress];
}
