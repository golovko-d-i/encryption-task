import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:encryption/data/provider/file_encryption_provider.dart';
import 'package:encryption/data/result.dart';
import 'package:encryption/resources/strings.dart';
import 'package:encryption/utils/error_codes.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'home_event.dart';

part 'home_state.dart';

const String _encryptionMethod = "AES/CBC/PKCS5PADDING";
const String _password = "mY_B3auT1fu1-p@ss";

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final FileEncryptionProvider _filePicker;

  HomeBloc({required FileEncryptionProvider filePicker})
      : _filePicker = filePicker,
        super(const HomeSelectFile());

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is SelectFileEvent) {
      yield* _mapSelectFile();
    } else if (event is FileSelectedEvent) {
      yield* _mapFileSelected(event.path);
    }
  }

  Stream<HomeState> _mapSelectFile() async* {
    try {
      var result = await _filePicker.pickFile();
      if (result is Success<String>) {
        // move to encryption stage
        add(FileSelectedEvent(result.result));
      } else if (result is Error<String>) {
        var errorText = _mapSelectFileError(result);
        if (errorText != null) {
          yield HomeSelectFile(message: errorText);
        }
      }
    } catch (e) {
      yield const HomeSelectFile(message: Strings.genericError);
    }
  }

  Stream<HomeState> _mapFileSelected(String filePath) async* {
    // method and password selection can be implemented on ui
    yield* _filePicker
        .encodeFile(filePath, _encryptionMethod, _password)
        .map((event) {
      if (event is Success<double>) {
        // done
        if (event.result == 1) {
          return const HomeSelectFile(message: Strings.encryptionComplete);
        } else {
          // in progress
          return HomeProgress((event.result * 100.0).toInt());
        }
      } else if (event is Error<double>) {
        var errorText = _mapEncryptFileError(event);
        if (errorText != null) {
          return HomeSelectFile(message: errorText);
        } else {
          // show file selector screen
          return const HomeSelectFile();
        }
      } else {
        // show file selector screen
        return const HomeSelectFile(message: Strings.genericError);
      }
    });
  }

  String? _mapSelectFileError(Error error) {
    switch (error.errorCode) {
      case ErrorCode.permissionRequestDeclined:
      case ErrorCode.userCancelled:
        return null;
      case ErrorCode.inaccessibleFile:
        return Strings.cannotAccessFile;
      default:
        return Strings.genericError;
    }
  }

  String? _mapEncryptFileError(Error error) {
    switch (error.errorCode) {
      case ErrorCode.anErrorOccurred:
        return Strings.genericError;
      default:
        return null;
    }
  }
}
