part of 'home_bloc.dart';

@immutable
abstract class HomeEvent extends Equatable {
  const HomeEvent();

  @override
  List<Object> get props => [];
}

class SelectFileEvent extends HomeEvent {}

@immutable
class FileSelectedEvent extends HomeEvent {

  final String path;
  const FileSelectedEvent(this.path);

  @override
  List<Object> get props => [path];

  @override
  String toString() {
    return 'FileSelectedEvent{path: $path}';
  }

}
