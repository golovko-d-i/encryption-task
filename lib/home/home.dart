import 'package:encryption/home/bloc/home_bloc.dart';
import 'package:encryption/resources/strings.dart';
import 'package:encryption/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  final _progressPercentFormat = NumberFormat("0");

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        return _getContent(context, state);
      },
    );
  }

  void _showMessage(BuildContext context, String message) {
    postDelayed(() {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(message),
        ),
      );
    });
  }

  /// Draws home screen content depending on the state
  /// List of states is here [HomeState]
  Widget _getContent(BuildContext context, HomeState state) {
    if (state is HomeSelectFile) {
      if (state.message != null) {
        _showMessage(context, state.message!);
      }
      return Center(
        child: ElevatedButton(
          onPressed: () {
            context.read<HomeBloc>().add(SelectFileEvent());
          },
          child: const Text(Strings.selectFileToEncrypt),
        ),
      );
    } else if (state is HomeProgress) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const CircularProgressIndicator(),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "${widget._progressPercentFormat.format(state.progress)}%",
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      );
    } else {
      return const Center(
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Text(
            Strings.homeStateError,
            textAlign: TextAlign.center,
          ),
        ),
      );
    }
  }
}
