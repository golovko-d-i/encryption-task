# Assignment Option 1: AES Encryption

Test project that encrypts selected file and puts it encrypted copy next to the original file.

## About

* app runs encryption in a separate thread to ensure smooth UI
* flutter part is implemented using BLOC architecture which makes it easier to maintain
* android part allows picking almost any file from filesystem in the case of the granted permission

